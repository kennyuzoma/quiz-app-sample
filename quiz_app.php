<?php

//required files
require('inc/connect.php');
require('inc/functions.php');
require('inc/config.php');

// empty the error variable and array
$error = FALSE;
$errors = array();

// email
if(isset($_POST['email']) && $_POST['email'] != '')
{
	// valid email address?
	if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
	{
		$error = TRUE;
		array_push($errors, 'Please enter a valid email address.');
	}
	else
	{
		$email = $_POST['email'];
	}
}
else
{
	$email = '';
}
//}


// Age
if((!isset($_POST['age'])) || ($_POST['age'] == ''))
{
	$error = TRUE;
	array_push($errors,  'Age is required.');
}
else
{
	$age = $_POST['age'];
}

// Gender
if((!isset($_POST['gender'])) || ($_POST['gender'] == ''))
{
	$error = TRUE;
	array_push($errors, 'Gender is required.');
}
else
{
	$gender = $_POST['gender'];
}


if((!isset($_POST['question_1'])) || ($_POST['question_1'] == ''))
{
	$question_1 = $_POST['question_1'] = '';
}
else
{
	$question_1 = $_POST['question_1'];
}


if((!isset($_POST['question_2'])) || ($_POST['question_2'] == ''))
{
	$question_2 = $_POST['question_2'] = '';
}
else
{
	$question_2 = $_POST['question_2'];
}


if((!isset($_POST['question_3'])) || ($_POST['question_3'] == ''))
{
	$question_3 = $_POST['question_3'] = '';
}
else
{
	$question_3 = $_POST['question_3'];
}


if((!isset($_POST['end_result'])) || ($_POST['end_result'] == ''))
{
	$end_result = $_POST['end_result'] = '';
}
else
{
	$end_result = $_POST['end_result'];
}


// add if statement for optin and email
// email is required if opted in
if((!isset($_POST['optin'])) || ($_POST['optin'] == ''))
{
	$optin = $_POST['optin'] = 0;
}
else
{
	$optin = $_POST['optin'];
}


if($error == TRUE)
{
	$json['status'] = 'error';
	$json['errors'] = $errors;
}
else
{
	// current time
	$date_created = date('Y-m-d H:i:s');

	// ins sql
	$ins = "INSERT INTO user_info
				(
					age,
					gender,
					email,
					optin,
					question_1,
					question_2,
					question_3,
					end_result,
					date_created
				)
			VALUES
				(?, ?, ?, ?, ?, ?, ?, ?, '{$date_created}')";

	// insert user
	$stmt = $mysqli->prepare($ins);
	$stmt->bind_param('sssissss', $age, $gender, $email, $optin, $question_1, $question_2, $question_3, $end_result);

	if(!$stmt->execute())
	{
		$json['status'] = 'error';

		if($GLOBALS['env'] != 'production')
		{
			$json['data']['message'] = 'MySQLi Error: ' . $stmt->error;
		}
		else
		{
			$json['data']['message'] = 'There is an error';
		}
	}
	else
	{
		$stmt->close();

		// json array
		$json['status'] = 'ok';
		$json['data']['message'] = 'The info was successfully saved to the database!';
	}
}

//echo json_encode($json, TRUE);

// For UPQA
echo json_encode($json, TRUE);